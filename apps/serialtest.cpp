#include <arducom/serialconnection.h>

int main()
{
    const char del { '\n' };
    arducom::SerialConnection conn("/dev/ttyACM0");

    conn.openPort(B9600);

    conn.findDelimiter = arducom::SerialConnection::getByteDelimiterFun(del);
    conn.readAsync();

    size_t i = 0;
    std::chrono::milliseconds duration { 5000 };
    auto start = std::chrono::high_resolution_clock::now();
    while (std::chrono::high_resolution_clock::now() - start < duration) {
        auto r = conn.getAsyncCallbackPromise<void>(
            [](std::optional<arducom::SerialConnection::BufferT> b) {
                if (b) {
                    std::cout << "got: " << std::string { b->begin(), b->end() }
                              << std::endl;
                }
            },
            std::chrono::milliseconds { 1100 });

        std::stringstream msg;
        msg << "test" << i++ << del;
        int written = conn.writeToPort(msg.str());
        std::cout << "wrote '" << msg.str() << "' (" << written << ")" << std::endl;

        if (std::future_status::timeout == r.wait_for(std::chrono::milliseconds { 1000 }))
            std::cout << "skipped" << std::endl;
    }

    conn.stopReadingAsync();
    std::cout << "stopped reading" << std::endl;
    conn.closePort();
    std::cout << "closed device" << std::endl;

    return 0;
}
