#include "serialconnection.h"

namespace arducom {

SerialConnection::SerialConnection(const std::filesystem::path& deviceFile)
    : deviceFile(deviceFile)
    , serialPort { -1 }
    , polling { false }
    , continueReading { false }
    , bufferChanged { false }
    , currentBuffer { std::make_unique<BufferT>() }
    , readBuffer { std::make_unique<BufferT>() }
{
}

SerialConnection::~SerialConnection()
{
    if (serialPort >= 0)
        closePort();
}

SerialConnection::FindDelimiterFunT SerialConnection::getByteDelimiterFun(char byte)
{
    return [byte](const BufferT& b) -> DelimiterItrT {
        auto itr = std::find(b.cbegin(), b.cend(), byte);
        return itr != b.cend() ? std::make_optional(itr) : std::nullopt;
    };
}

std::filesystem::path SerialConnection::getDeviceFile() const
{
    return deviceFile;
}

bool SerialConnection::isBufferChanged() const { return bufferChanged; }

SerialConnection::BufferT SerialConnection::getReadBuffer() const
{
    bufferChanged = false;

    std::scoped_lock<std::mutex> lock { bufferMutex };
    return *readBuffer;
}

void SerialConnection::openPort(speed_t baudrate)
{
    if (serialPort >= 0)
        closePort();

    if (!std::filesystem::exists(deviceFile) || !std::filesystem::is_character_file(deviceFile))
        throw std::runtime_error("No such device file '" + deviceFile.string() + "' or no character device");

    serialPort = open(deviceFile.c_str(), O_RDWR | O_NOCTTY | O_NDELAY);
    if (serialPort < 0) {
        std::stringstream msg;
        msg << "Could not open serial device " << deviceFile.string() << " (" << errno << " - "
            << std::strerror(errno) << ")";
        throw std::runtime_error(msg.str());
    }

    struct termios tty;
    if (tcgetattr(serialPort, &tty) != 0) {
        std::stringstream msg;
        msg << "Could get serial terminal attributes " << deviceFile.string() << " (" << errno
            << " - " << std::strerror(errno) << ")";
        throw std::runtime_error(msg.str());
    }

    tty.c_cflag &= ~PARENB; // disable generation and detection of the parity bit
    tty.c_cflag &= ~CSTOPB; // use single stop bit
    tty.c_cflag &= ~CSIZE; // clear size-bits
    tty.c_cflag |= CS8; // use 8-bits per byte
    tty.c_cflag &= ~CRTSCTS; // disable hardware flow control
    tty.c_cflag |= CREAD | CLOCAL; // Turn on READ & ignore ctrl lines (CLOCAL = 1)
    tty.c_lflag &= ~ICANON; // disable line-by-line processing (like a unix
                            // terminal/tty would do)
    tty.c_lflag &= ~ECHO; // Disable echo
    tty.c_lflag &= ~ECHOE; // Disable erasure
    tty.c_lflag &= ~ECHONL; // Disable new-line echo
    tty.c_lflag &= ~ISIG; // Disable interpretation of INTR, QUIT and SUSP
    tty.c_iflag &= ~(IXON | IXOFF | IXANY); // Turn off s/w flow ctrl
    tty.c_iflag &= ~(IGNBRK | BRKINT | PARMRK | ISTRIP | INLCR | IGNCR | ICRNL); // Disable any special handling of received bytes
    tty.c_oflag &= ~OPOST; // Prevent special interpretation of output bytes
                           // (e.g. newline chars)
    tty.c_oflag &= ~ONLCR; // Prevent conversion of newline to carriage return/line feed

    tty.c_cc[VMIN] = 0;
    tty.c_cc[VTIME] = 0;

    // Set in/out baudrate
    cfsetispeed(&tty, baudrate);
    cfsetospeed(&tty, baudrate);

    if (tcsetattr(serialPort, TCSANOW, &tty) != 0) {
        std::stringstream msg;
        msg << "Could set serial terminal attributes " << deviceFile.string() << " (" << errno
            << " - " << strerror(errno) << ")";
        throw std::runtime_error(msg.str());
    }
}

void SerialConnection::closePort()
{
    stopReadingAsync();
    close(serialPort);

    std::scoped_lock<std::mutex> lock(bufferMutex);
    serialPort = -1;
    readBuffer->clear();
    currentBuffer->clear();
    bufferChanged = false;
}

int SerialConnection::writeToPort(const std::vector<char>& data)
{
    ssize_t written = write(serialPort, data.data(), data.size());
    if (written < 0) {
        std::stringstream msg;
        msg << "Could not write serial (" << errno << " - " << strerror(errno)
            << ")";
        throw std::runtime_error(msg.str());
    }
    return written;
}

int SerialConnection::writeToPort(const std::string& data)
{
    ssize_t written = write(serialPort, data.data(), data.size());
    if (written < 0) {
        std::stringstream msg;
        msg << "Could not write serial (" << errno << " - " << strerror(errno)
            << ")";
        throw std::runtime_error(msg.str());
    }
    return written;
}

void SerialConnection::stopReadingAsync()
{
    continueReading = false;
    while (polling)
        std::this_thread::sleep_for(std::chrono::milliseconds(1));
}

} // namespace arducom
