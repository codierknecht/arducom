#ifndef SERIALCONNECTION_H
#define SERIALCONNECTION_H

#include <errno.h>
#include <fcntl.h>
#include <filesystem>
#include <poll.h>
#include <termios.h>

#include <cstring>
#include <functional>
#include <future>
#include <iostream>
#include <memory>
#include <optional>
#include <sstream>
#include <vector>

namespace arducom {

/*!
 * \brief Establishes connection to character-device representing an Arduino as serial-deivce.
 */
class SerialConnection {
public:
    using BufferT = std::vector<char>;
    using BufferPtrT = std::unique_ptr<BufferT>;
    using DelimiterItrT = std::optional<BufferT::const_iterator>;
    using FindDelimiterFunT = std::function<DelimiterItrT(const BufferT&)>;

    explicit SerialConnection(const std::filesystem::path& deviceFile);
    ~SerialConnection();

    /*!
     * \brief Functor used during async read to decide if a message is complete and can be set to
     * readBuffer.
     * \note return nullopt message is not complete or expeced end of message was not found,
     * delimiter position otherwise
     */
    FindDelimiterFunT findDelimiter { [](const BufferT&) { return std::nullopt; } };

    /*!
     * \brief creates functor searching for the passed delimiter byte in passed buffer
     * returning const iterator to delimiter position or nullopt if not found.
     * \param byte delimiter to look for
     * \return delimiter-serach-functor e.g. \ref SerialConnection::findDelimiter
     */
    static FindDelimiterFunT getByteDelimiterFun(char byte);

    /*!
     * \brief Waits asyncron for readBuffer to get ready to read for a parameterizable max duration.
     * \note readBuffer is passed to callback as it gets ready or nullopt in case of timeout
     * \param callback functor to be called as soon as either readBuffer is ready or timeout
     * \param bufferChangedTimeout timeout to limit runtime of asynchronous waiting
     * \return asynchronous callback result
     */
    template <typename ResultT>
    std::future<ResultT> getAsyncCallbackPromise(
        std::function<ResultT(std::optional<BufferT>)> callback,
        std::chrono::milliseconds bufferChangedTimeout = std::chrono::milliseconds { 10000 });

    std::filesystem::path getDeviceFile() const;

    /*!
     * \brief Indicates if buffer has changed since last \ref SerialConnection::getReadBuffer() call.
     */
    bool isBufferChanged() const;
    /*!
     * \brief Provides access to readBuffer and resets buffer-changed flag.
     * \return copy of readBuffer
     */
    BufferT getReadBuffer() const;

    /*!
     * \brief Openes device file to read and write.
     * \param baudrate serial connection speed
     * \throws \ref std::runtime_error if any issues occur during opening the device file or configuring
     * it as serial device
     */
    void openPort(speed_t baudrate);
    /*!
     * \brief Closes device file if open and stops async reading if necessary.
     */
    void closePort();

    /*!
     * \brief Write a set of bites to the connected character device.
     * \param data bytes to send
     * \return number of bytes actually sent
     */
    int writeToPort(const std::vector<char>& data);
    /*!
     * \copydoc SerialConnection::writeToPort(const std::vector<char>&)
     */
    int writeToPort(const std::string& data);

    /*!
     * \brief Read available bytes from character device.
     * \return read bytes
     */
    template <bool AddNullterm = false>
    BufferT readFromPort();
    /*!
     * \brief Constantly runs asyncrone reads until a message is considered complete according to
     * \ref SerialConnection::findDilimiter and writes it to readBuffer accessed through
     * \ref SerialConnection::getReadBuffer(). Also sets buffer-changed flag true as soon as
     * message is complete.
     * \note this creates frequent access to the opend device file
     */
    template <bool KeepDelimiter = false>
    void readAsync();
    /*!
     * \brief Stops asyncrone read-proccess.
     * \note blocks until reading is actually stoped
     */
    void stopReadingAsync();

private:
    const std::filesystem::path deviceFile;

    int serialPort;
    std::atomic_bool polling;
    std::atomic_bool continueReading;
    mutable std::atomic_bool bufferChanged;
    BufferPtrT currentBuffer;
    BufferPtrT readBuffer;
    mutable std::mutex bufferMutex;
};

template <typename ResultT>
std::future<ResultT> SerialConnection::getAsyncCallbackPromise(
    std::function<ResultT(std::optional<BufferT>)> callback,
    std::chrono::milliseconds bufferChangedTimeout)
{
    return std::async(
        std::launch::async, [this, callback, bufferChangedTimeout]() {
            auto dur = [start = std::chrono::high_resolution_clock::now()]() {
                return std::chrono::high_resolution_clock::now() - start;
            };
            while (isBufferChanged() == false && dur() < bufferChangedTimeout)
                std::this_thread::sleep_for(std::chrono::milliseconds { 1 });

            return callback(isBufferChanged() ? std::make_optional(getReadBuffer())
                                              : std::nullopt);
        });
}

template <bool AddNullterm>
SerialConnection::BufferT SerialConnection::readFromPort()
{
    char buffer[255] = { 0 };
    ssize_t bytesRead = read(serialPort, buffer, sizeof(buffer));

    if (bytesRead > 0) {
        if constexpr (AddNullterm) {
            BufferT b(bytesRead + 1, 0);
            b.insert(b.end(), &buffer[0], &buffer[bytesRead]);
            return b;

        } else {
            return BufferT { &buffer[0], &buffer[bytesRead] };
        }

    } else if (bytesRead < 0) {
        std::stringstream msg;
        msg << "Could not read from serial (" << errno << " - " << strerror(errno)
            << ")";
        throw std::runtime_error(msg.str());
    }

    return BufferT {};
}

template <bool KeepDelimiter>
void SerialConnection::readAsync()
{
    polling = true;
    continueReading = true;

    std::thread t { [this]() {
        while (continueReading && serialPort > 0) {
            struct pollfd fds[1];
            memset(fds, 0, sizeof(fds));

            fds[0].fd = serialPort;
            fds[0].events = POLLIN;
            int rc = poll(fds, 1, 10);

            if (rc < 0) {
                std::stringstream msg;
                msg << "Could not async read from serial (" << errno << " - "
                    << strerror(errno) << ")";
                throw std::runtime_error(msg.str());

            } else if (fds[0].revents & POLLIN) {
                const BufferT read = readFromPort();
                DelimiterItrT stopAt = findDelimiter(read);

                currentBuffer->insert(currentBuffer->end(), read.cbegin(),
                    stopAt.value_or(read.cend()));

                if (stopAt) {
                    {
                        std::scoped_lock<std::mutex> lock { bufferMutex };
                        std::swap(currentBuffer, readBuffer);
                    }

                    currentBuffer = std::make_unique<BufferT>();
                    if (*stopAt != read.cend()) {
                        if constexpr (KeepDelimiter) {
                            currentBuffer->insert(currentBuffer->end(), *stopAt, read.cend());

                        } else {
                            auto startAt { std::next(*stopAt) };
                            currentBuffer->insert(currentBuffer->end(), startAt, read.cend());
                        }
                    }

                    bufferChanged = true;
                }
            }

            std::this_thread::sleep_for(std::chrono::milliseconds { 5 });
        }

        polling = false;
    } };

    t.detach();
}

} // namespace arducom

#endif // SERIALCONNECTION_H
